require imx-gpu-viv-v6.inc

SRC_URI[md5sum] = "8f7c97654c89ffd9e4cb2dc5a41eb7cb"
SRC_URI[sha256sum] = "a47e157b3571dbe7b00bce6552d2d04b0a2ae86649a1934bcbee70bf31fb66bf"

COMPATIBLE_MACHINE = "(mx8)"
